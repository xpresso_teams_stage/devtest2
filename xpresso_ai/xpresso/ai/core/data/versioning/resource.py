class PachydermResource(object):
    """
    base class for resources on pachyderm
    """

    def __init__(self):
        """
        constructor class for PachydermResource
        """
        pass

    def create(self, *args):
        """
        creates a new resource item on pachyderm
        :param args:
        :return:
        """
        pass

    def list(self, *args):
        """
        lists all items of a specified resource
        :param args:
        :return:
        """
        pass

    def inspect(self, *args):
        """
        inspects a specific resource item
        :param args:
        :return:
        """
        pass

    def delete(self, *args):
        """
        deletes a resource item from the cluster
        :param args:
        :return:
        """
        pass

    def filter_output(self, *args):
        """
        filters the output received from pachyderm cluster

        generates a user friendly output rather that sending pachyderm output
        directly to the user
        :param args:
        :return:
        """
        pass

    # TODO: Add update resource in future release if required
