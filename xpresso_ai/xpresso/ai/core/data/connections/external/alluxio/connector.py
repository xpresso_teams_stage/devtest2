""" FSConnector class for connectivity to various filesystems """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import os
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.abstract_fs import AbstractFSConnector
from xpresso.ai.core.data.connections.external.alluxio import client as alluxio
from xpresso.ai.core.logging.xpr_log import XprLogger


class AlluxioFSConnector(AbstractFSConnector):
    """

    DataConnector class for connecting to filesystems.

    """

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API.

        """

        self.client = alluxio.AlluxioConnector()

    def getlogger(self):
        """

        Xpresso logger built on top of python module.

        """

        return XprLogger()

    def connect(self, config):
        """

        Connect method to establish client-side connection with a filesystem.

        Args:
            config (dict): A JSON object, input by the user, that states
                the file to be imported.

        Returns:
            path (str): absolute path to a target file/directory.
            dataset_type (str): to identify distributed dataset.

        """

        path = config.get(constants.input_path)
        dataset_type = config.get(constants.dataset_type)
        return path, dataset_type

    def import_dataframe(self, config, **kwargs):
        """

        Importing data from a user specified file in a filesystem.

        Args:
            config (dict): A JSON object, input by the user, that states
                the file to be imported.

        Returns:
            object: a pandas DataFrame.

        """

        path, dataset_type = self.connect(config)
        try:
            data_frame = self.client.import_data(path, dataset_type, **kwargs)
            self.getlogger().info("Data imported from filesystem.")
        except Exception:
            self.getlogger().error("Data import failed")
            raise xpr_exp.DataConnectionsException
        finally:
            self.close()
        return data_frame

    def import_files(self, config):
        """

        Importing file/files from a user specified filesystem.

        Args:
            config (dict): A JSON object, input by the user, that states
                the file/files to be imported.

        Returns:
            object: a pandas DataFrame.

        """

        data_frame = None
        try:
            path, dataset_type = self.connect(config)
            if not self.client.alluxio_client().exists(path):
                message = f"Path {path} does not exist. " \
                          f"Please mount the path on Alluxio"
                self.getlogger().info(message)
                raise xpr_exp.InvalidPathException(message=message)
            file_extension = os.path.splitext(path)[1]
            if file_extension in constants.csv_extension or \
                    file_extension in constants.excel_extension:
                data_frame = self.client.save_file_raw(path)
                self.getlogger().info("File %s has been imported." % path)
            else:
                try:
                    if file_extension[0] == constants.DOT:
                        self.getlogger().error("Error: Unsupported Filetype found")
                except IndexError:
                    data_frame = self.client.save_multiple_files_raw(path)
                    self.getlogger().info(
                        "Files from %s directory have been imported." % path
                    )
        except Exception:
            self.getlogger().error("File import failed")
            raise xpr_exp.DataConnectionsException
        finally:
            self.close()
        return data_frame

    def list_fs(self, path: str):
        """
            Method to list files on all filesystems
            mounted under Data connections
        Args:
            path(str): path to list directory
        Returns:
            list of strings: A list of names of the files and
                directories under path.
        """

        return self.client.ls(path)

    def mount_fs(self, path: str, src: str, **kwargs):
        """
            Method to mount a filesystem.
        Args:
            path(str): path on Remote FS where target FS will be mounted
            src(str): source URI for target filesystem
        Keyword Args:
            properties(dict): A dictionary mapping property key strings to value strings.
            read_only (bool): Whether the mount point is read-only.
            shared (bool): Whether the mount point is shared with all users.

        Example: In case of mounting an S3 bucket;
            kwarg = {
                "properties": {
                    "aws.accessKeyId": "<accessKeyId1>",
                    "aws.secretKey": "<secretKey1>"
                },
                "read_only": False,
                "shared": True
            }
        """

        return self.client.mount(path, src, **kwargs)

    def unmount_fs(self, path: str):
        """
            Method to unmount a filesystem.
        Args:
            path(str): path to understorage to be unmounted
        """

        self.client.unmount(path)

    def close(self):
        """

        Method to close connection to filesystem.

        """

        self.client.close()
